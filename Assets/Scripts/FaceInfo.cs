using System;
using UnityEngine;

/// <summary>
/// Given ID is the index of the Connection Data
/// </summary>
[System.Serializable]
public class FaceInfo 
{
	public FaceInfo Copy()
	{
		return new FaceInfo
		           {
			           Forward = Forward,
			           Right = Right,
			           Backward = Backward,
			           Left = Left
		           };
	}

	public FaceInfo ShiftCopy(int shift)
	{
		return new FaceInfo
		       {
			       Forward = GetIdCycledAt(0 + shift),
			       Right = GetIdCycledAt(1 + shift),
			       Backward = GetIdCycledAt(2 + shift),
			       Left = GetIdCycledAt(3 + shift)
		       };

		int GetIdCycledAt(int index)
		{
			return this[(int)Mathf.Repeat(index, 4)];
		}
	}

	// Rotated clockwise
	public int Forward = -1;
	public int Right = -1;
	public int Backward = -1;
	public int Left = -1;

	public int this[int index]
	{
		get
		{
			switch (index)
			{
				case 0: return Forward;
				case 1: return Right;
				case 2: return Backward;
				case 3: return Left;
			}

			return 0;
		}
		set
		{
			switch (index)
			{
				case 0:
					Forward = value;
					break;
				case 1:
					Right = value;
					break;
				case 2:
					Backward = value;
					break;
				case 3:
					Left = value;
					break;
			}
		}
	}

	public bool Equals(FaceInfo other)
	{
		return other.Forward == Forward && 
			other.Right == Right &&
			other.Backward == Backward &&
			other.Left == Left;
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;
		return Equals((FaceInfo) obj);
	}
}