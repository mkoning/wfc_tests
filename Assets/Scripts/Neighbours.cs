using System;
using System.Collections.Generic;

[System.Serializable]
public class Neighbours
{
	public MapSlot Left;
	public MapSlot Right;
	public MapSlot Forward;
	public MapSlot Backward;

	private List<MapSlot> _neighbours;
	public List<MapSlot> All
	{
		get
		{
			if (_neighbours == null)
			{
				_neighbours = new List<MapSlot>();
				if (Left != null) _neighbours.Add(Left);
				if (Right != null) _neighbours.Add(Right);
				if (Forward != null) _neighbours.Add(Forward);
				if (Backward != null) _neighbours.Add(Backward);
			}
			return _neighbours;
		}
	}
}

public static class NeighboursExtension
{
	public static Direction GetDirectionTo(this Neighbours neighbours, MapSlot slot)
	{
		if (slot == null)
		{
			throw new NullReferenceException("Slot can not be null");
		}

		if (neighbours.Left == slot) return Direction.Left;
		if (neighbours.Right == slot) return Direction.Right;
		if (neighbours.Forward == slot) return Direction.Forward;
		if (neighbours.Backward == slot) return Direction.Backward;

		throw new Exception($"No direction found for {slot}");
	}

	public static bool Contains(this Neighbours neighbours, MapSlot slot)
	{
		return neighbours.All.Contains(slot);
	}
}