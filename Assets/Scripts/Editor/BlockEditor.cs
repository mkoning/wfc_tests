using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Block))][CanEditMultipleObjects]
public class BlockEditor : Editor
{
	private Block _block;

	private void OnEnable()
	{
		_block = (Block)target;
	}

	private void OnDisable()
	{
		Block.CurrentSelectedId = -1;
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		DrawPropertiesExcluding(serializedObject, "_faceInfo", "_rotations");
		GUI.enabled = false;
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_faceInfo"), true);
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_rotations"), true);

		GUI.enabled = true;


		if (GUILayout.Button("Add To ModuleData", EditorStyles.miniButton))
		{
			var data = Resources.Load<ModuleData>("Module Data");
			if (data == null)
			{
				Debug.Log("Failed to find Module Data");
				return;
			}

			var serializedData = new SerializedObject(data);
			var prefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(_block);

			if (prefab == null)
			{
				Debug.LogError("Could not find the correct prefab");
				return;
			}

			var rotations = serializedObject.FindProperty("_rotations");

			for (int i = 0; i < rotations.arraySize; i++)
			{
				var valueAtIndex = rotations.GetArrayElementAtIndex(i).intValue;
				var hasVersion = data.Modules.Any(d => d.Block == prefab && d.Rotation == valueAtIndex);
				if (hasVersion)
				{
					Debug.Log($"Already has version with rotation: {valueAtIndex}");
					// Already a prefab with this rotation in the module list.
					// Change the probability of there needs to be more chance for this object.
					return;
				}

				data.AddModule(prefab, i);
			}

			serializedData.ApplyModifiedProperties();
		}

		if (GUILayout.Button("Remove from ModuleData", EditorStyles.miniButton))
		{
			var data = Resources.Load<ModuleData>("Module Data");
			if (data == null)
			{
				Debug.Log("Failed to find Module Data");
				return;
			}

			var serializedData = new SerializedObject(data);
			var prefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(_block);

			if (prefab == null)
			{
				Debug.LogError("Could not find the correct prefab");
				return;
			}

			for (var i = data.Modules.Count - 1; i > 0; i--)
			{
				var module = data.Modules[i];
				if (module.Block == prefab)
				{
					data.Modules.Remove(module);
				}
			}

			serializedData.ApplyModifiedProperties();
		}

		serializedObject.ApplyModifiedProperties();
	}

	protected virtual void OnSceneGUI()
	{
		float buttonSize = 0.5f;


		for (int i = 0; i < 4; i++)
		{
			Vector3 position = _block.transform.position + DebugPositions.GetPosition(i);
			if (Handles.Button(position, Quaternion.identity, buttonSize, buttonSize, Handles.SphereHandleCap))
			{
				var id = _block.Faces[i];
				if (Block.CurrentSelectedId == id)
				{
					Block.CurrentSelectedId = -1;
				}
				else
				{
					Block.CurrentSelectedId = id;
				}
			}
		}
	}
}
