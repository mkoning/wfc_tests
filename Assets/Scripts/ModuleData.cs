using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ModuleData : ScriptableObject
{
	public List<Module> Modules = new List<Module>();
	public int ModuleCount => Modules.Count;
	public void AddModule(Block block, int rotation)
	{
		Modules.Add(new Module(block, rotation));
	}
}
