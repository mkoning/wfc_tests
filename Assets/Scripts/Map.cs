using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Map : MonoBehaviour
{
	[SerializeField][Range(1, 100)] private int _width = 1;
	[SerializeField][Range(1, 100)] private int _depth = 1;
	[SerializeField] private ModuleData _moduleData;
	MapSlot[] _mapOptions;

	Stack<MapSlot> _options = new Stack<MapSlot>();

	void Start()
	{
		var blocksize = Block.BLOCK_SIZE;
		_mapOptions = new MapSlot[_width * _depth];

		for (int i = 0, x = 0; x < _width; x++)
		{
			for (int z = 0; z < _depth; z++, i++)
			{
				var xPosition = -((_width * blocksize)/2f) + (x * blocksize) + (blocksize/2f);
				var zPosition = -((_depth * blocksize)/2f) + ( z * blocksize) + (blocksize/2f);
				var option = new GameObject($"Option [{x}, {z}]");
				option.transform.SetParent(transform);
				option.transform.position = new Vector3(xPosition, 0, zPosition);
				_mapOptions[i] = option.AddComponent<MapSlot>();
				_mapOptions[i].Initialize(this, _moduleData);
			}
		}

		for (var i = 0; i < _mapOptions.Length; i++)
		{
			AssignNeighbours(_mapOptions[i], i);
		}

		StartCoroutine(RunCollapse());
	}

	IEnumerator RunCollapse()
	{
		while (IsFullyCollapsed() == false)
		{
			yield return null;


			var options = new List<MapSlot>();
			var lowest = float.MaxValue;

			foreach (var option in _mapOptions)
			{
				if (option.IsFullyCollapsed) continue;

				if (option.Entropy > lowest) continue;
				
				if (option.Entropy < lowest)
				{
					lowest = option.Entropy;
					options.Clear();
					options.Add(option);
				}
				options.Add(option);
			}

			var slot = options.GetRandomElement();


			if (slot == null)
			{
				Debug.LogError("Failed to find a slot");
				yield break;
			}

			// 2. Collapse the wavefunction at these co-ordinates
			slot.Collapse();

			// 3. Propagate the consequences of this collapse
			
			yield return Propagate(slot);
		}
	}

	private bool IsFullyCollapsed()
	{
		return _mapOptions.Any(slot => slot.IsFullyCollapsed == false) == false;
	}

	/// <summary>
	/// Propagates the consequences of the wavefunction at `slot` collapsing.
	/// If the wavefunction collapses to a fixed tile,
	/// then some tiles may not longer be theoretically possible at surrounding locations.
	/// This method keeps propagating the consequences of the consequences, and so on until no consequences remain.
	/// </summary>
	/// <param name="mapSlot"></param>
	/// <returns></returns>
	IEnumerator Propagate(MapSlot mapSlot)
	{
		_options = new Stack<MapSlot>();

		_options.Push(mapSlot);

		yield return null;

		while (_options.Count > 0)
		{
			// Get a current slot
			var slot = _options.Pop();
			
			var neighbours = slot.Neighbours.All;
			foreach (var neighbourSlot in neighbours)
			{
				if (neighbourSlot.IsFullyCollapsed)
				{
					continue;
				}
				// Check the neighbourSlot against the current Slot.
				// Update the neighbour and push it to the stack to propagate its changes to their neighbours 
				var hasChanges = neighbourSlot.UpdateSlotOptions(slot);
				if (hasChanges)
				{
					_options.Push(neighbourSlot);
				}
			}
			yield return null;
		}
	}

	private void AssignNeighbours(MapSlot slot, int index)
	{
		var neighbours = new Neighbours();
		neighbours.Left = GetSlotAt(index - _depth);
		neighbours.Right = GetSlotAt(index + _depth);
		neighbours.Forward = GetSlotAt(index + 1);
		neighbours.Backward = GetSlotAt(index - 1);

		slot.AssignNeighbours(neighbours);
	}

	private MapSlot GetSlotAt(int index)
	{
		if (index < _mapOptions.Length && index >= 0)
		{
			return _mapOptions[index];
		}
		return null;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(transform.position, new Vector3(_width * Block.BLOCK_SIZE, 0, _depth * Block.BLOCK_SIZE));
	}
}
