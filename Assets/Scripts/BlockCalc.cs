using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class BlockCalc
{
	public static Vector2[] GetFace(Mesh mesh, Vector3 direction)
	{
		var halfSize = Block.BLOCK_SIZE / 2f;
		var center = new Vector3(-halfSize, 0, -halfSize);
		var rotation = Quaternion.Inverse(Quaternion.LookRotation(direction));
		var vertices = mesh.vertices.Select(v => rotation * (v - center)).ToArray();

		var result = new HashSet<Vector2>();

		for (int i = 0; i < vertices.Length; i++)
		{
			var v = vertices[i];
			if (v.z > halfSize * 0.99f)
			{
				result.Add(new Vector2(Truncate(v.x, 4), Truncate(v.y, 4)));
			}
		}

		Debug.Log($"{rotation} results in {result.Count} points");
		return result.ToArray();
	}

	public static bool Match(Vector2[] a, Vector2[] b)
	{
		const float threshold = 0.001f;
		const float matchPercentage = 0.8f;
		int c1 = a.Count(v1 => b.Any(v2 => Vector2.Distance(v1, v2) < threshold));
		int c2 = b.Count(v1 => a.Any(v2 => Vector2.Distance(v1, v2) < threshold));

		return (!a.Any() || c1 >= Mathf.FloorToInt(a.Count() * matchPercentage) + 1) && 
			   (!b.Any() || c2 >= Mathf.FloorToInt(b.Count() * matchPercentage) + 1);
	}

	public static float Truncate(float value, int digits)
	{
		var mult = Mathf.Pow(10.0f, digits);
		return Mathf.Round(value * mult) / mult;
	}
}


