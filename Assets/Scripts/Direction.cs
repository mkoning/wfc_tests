public enum Direction
{
	Left,
	Forward,
	Right,
	Backward,
}

public static class DirectionExtensions
{
	public static Direction Inverse(this Direction direction)
	{
		var m = 4; // enum length
		var x = (int) direction - 2;
		var r = x % m;

		return r < 0 ? (Direction) r + m : (Direction) r;
	}
}