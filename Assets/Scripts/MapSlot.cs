using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapSlot : MonoBehaviour
{
	// We only save the index based on the moduleData options. No need create arrays filled with loads of data
	[SerializeField] private bool[] _optionsTaken;
	private ModuleData _moduleData;
	private Map _map;
	private bool _isEntropyDirty = true;
	private float _entropy;
	public bool[] OptionsTaken => _optionsTaken;

	public Neighbours Neighbours;// { get; private set; }

	public float Entropy
	{
		get
		{
			if (_isEntropyDirty)
			{
				_entropy = GetEntropy();
			}

			return _entropy;
		}
	}

	public bool IsFullyCollapsed { get; private set; } = false;

	public void Initialize(Map map, ModuleData moduleData)
	{
		_moduleData = moduleData;

		// Note that all values will be set to [false]. So [true] means that this option is removed
		_optionsTaken = new bool[moduleData.ModuleCount];
		_entropy = GetEntropy();
		IsFullyCollapsed = false;
	}

	public void AssignNeighbours(Neighbours neighbours)
	{
		if (Neighbours != null)
		{
			Debug.LogWarning($"It seems you already assigned neightbours for slot {gameObject.name}, will still assign the new neighbours");
		}

		Neighbours = neighbours;
	}

	/// <summary>
	/// Collapses the mapOption to a single, definite tile.
	/// The tile is chosen randomly from the remaining possible options weighted according to the models weight
	/// </summary>
	public void Collapse()
	{
		// select the indexes of the possible entries in _optionsTaken
		var selectableOptions = _optionsTaken.Select((isOptionTaken, index) => (isOptionTaken, index))
											 .Where(t => t.isOptionTaken == false)
											 .Select(t => (t.index, _moduleData.Modules[t.index].Probability))
											 .ToArray();

		if (selectableOptions.Length == 0)
		{
			Debug.LogError($"{gameObject.name} options: {_optionsTaken.ToDebugString()}");
		}

		var total = selectableOptions.Sum(t => t.Probability);
		var chosen = Random.Range(0, total);
		var running = 0f;
		Module chosenModule = null;




		foreach (var option in selectableOptions)
		{

			running += option.Probability;

			if (chosen < running && chosenModule == null)
			{
				chosenModule = _moduleData.Modules[option.index];
			}
			else
			{
				RemoveOption(option.index);
			}
		}

		SpawnBlock(chosenModule);

		IsFullyCollapsed = true;
		_entropy = 0;
	}

	private void SpawnBlock(Module module)
	{
		var block = Instantiate(module.Block, transform, false);
		block.transform.position = new Vector3(transform.position.x + Block.BLOCK_SIZE / 2f,
		                                       transform.position.y,
		                                       transform.position.z + Block.BLOCK_SIZE / 2f);

		block.transform.RotateAround(transform.position, Vector3.up, module.Rotation * -90);
	}

	/// <summary>
	/// Update the options based on the given neighbour
	/// </summary>
	public bool UpdateSlotOptions(MapSlot fromSlot)
	{
		var shouldUpdateSlot = false;

		var fromDirection = fromSlot.Neighbours.GetDirectionTo(this);
		//Debug.Log($"from Slot {fromSlot.gameObject.name} from direction {fromDirection}");

		var availableNeighbourIds = fromSlot.OptionsTaken.Select((b, index) => (b, index))
		                .Where(t => t.b == false)
		                .Select(t => Module(t.index).GetIdForDirection(fromDirection))
		                .Distinct()
		                .ToArray();

		// for each option in this slot, check against the available ids
		// if the option does not contain any of the ids, mark the option as removed
		for (int i = 0; i < _optionsTaken.Length; i++)
		{
			if (_optionsTaken[i] == true)
			{
				// this option is already removed
				continue;
			}

			var myId = Module(i).GetIdForDirection(fromDirection.Inverse());
			if (availableNeighbourIds.Contains(myId))
			{
				// do nothing
			}
			else
			{
				RemoveOption(i);
				shouldUpdateSlot = true;
			}
		}

		//Debug.Log($"{gameObject.name} Remove options: {removedOptions.ToDebugString()}");
		
		return shouldUpdateSlot;
	}

	private Module Module(int index) => _moduleData.Modules[index];

	public void RemoveOption(int moduleIndex)
	{
		_optionsTaken[moduleIndex] = true;
		_isEntropyDirty = true;
	}

	private float GetEntropy()
	{
		float weight = 0;
		float entropySum = 0;

		var modules = _moduleData.Modules;

		for (int i = 0; i < _optionsTaken.Length; i++)
		{
			if (_optionsTaken[i] == true)
			{
				continue;
			}

			var module = modules[i];
			var probability = module.Probability;
			weight += probability;
			entropySum += probability * Mathf.Log(probability);
		}

		return Mathf.Log(weight) - (entropySum / weight);
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawSphere(transform.position, 1 * _entropy);
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		if (Neighbours != null)
		{
			foreach (var slot in Neighbours.All)
			{
				Gizmos.DrawSphere(slot.transform.position, 1);
			}
		}
	}
}