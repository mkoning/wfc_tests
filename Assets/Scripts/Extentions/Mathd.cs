using System;

/// <summary>
/// Math class implementing some helper functions for doubles found in Unity's Mathf
/// </summary>
public static class Mathd
{
	/// <summary>
	/// Clamp value between min and max
	/// </summary>
	public static double Clamp(double value, double min, double max)
	{
		if (value < min) return min;
		if (value > max) return max;

		return value;
	}

	/// <summary>
	/// Clamp value between 0 and 1
	/// </summary>
	public static double Clamp01(double value)
	{
		if (value < 0.0) return 0.0;
		if (value > 1.0) return 1;

		return value;
	}

	/// <summary>
	/// Linearly interpolates between a and b by t, where t is clamped between 0 and 1
	/// </summary>
	public static double Lerp(double a, double b, double t)
	{
		return a + (b - a) * Clamp01(t);
	}

	/// <summary>
	/// Linearly interpolates between a and b by t.
	/// </summary>
	public static double LerpUnclamped(double a, double b, double t)
	{
		return a + (b - a) * t;
	}

	/// <summary>
	/// Are a and b approximately equal?
	/// </summary>
	public static bool Approximately(double a, double b)
	{
		return Math.Abs(b - a) < double.Epsilon * 8;
	}
}
