﻿using System;
using UnityEngine;

namespace Temple
{
	public static class NumberExt
	{
		public static double FloorTo(this double value, double precision)
		{
			return Math.Floor(value / precision) * precision;
		}

		public static double RoundTo(this double value, double round)
		{
			return Math.Round(value / round) * round;
		}

		public static float FloorTo(this float value, float precision)
		{
			return Mathf.Floor(value / precision) * precision;
		}

		public static float RoundTo(this float value, float round)
		{
			return Mathf.Round(value / round) * round;
		}

		public static int RoundTo(this int value, int round)
		{
			return Mathf.RoundToInt((float) value / round) * round;
		}

		public static int RoundTo(this float value, int round)
		{
			return Mathf.RoundToInt(value / round) * round;
		}

		public static float Remap(this int value, float minFrom, float maxFrom, float minTo, float maxTo)
		{
			return Remap((float) value, minFrom, maxFrom, minTo, maxTo);
		}

		public static int Remap(this int value, float minFrom, float maxFrom, int minTo, int maxTo)
		{
			return (int) Remap((float) value, minFrom, maxFrom, minTo, maxTo);
		}

		public static float Remap(this float value, float minFrom, float maxFrom, float minTo, float maxTo)
		{
			return minTo + (value - minFrom) * (maxTo - minTo) / (maxFrom - minFrom);
		}

		public static float Remap(this float value, double minFrom, double maxFrom, double minTo, double maxTo)
		{
			return (float) (minTo + (value - minFrom) * (maxTo - minTo) / (maxFrom - minFrom));
		}

		public static double Remap(this double value, double minFrom, double maxFrom, double minTo, double maxTo)
		{
			return (minTo + (value - minFrom) * (maxTo - minTo) / (maxFrom - minFrom));
		}

		public static float WrapAngle(this float value)
		{
			var num = Mathf.Repeat(value, 360f);
			if (num > 180.0f) num -= 360f;

			return num;
		}

		public static Vector3 WrapAngle(this Vector3 value)
		{
			for (int i = 0; i < 2; i++)
			{
				value[i] = value[i].WrapAngle();
			}

			return value;
		}

		public static int ParseIntOrDefault(this string input, int defaultValue = 0)
		{
			int ret = defaultValue;

			int.TryParse(input, out ret);

			return ret;
		}

		public static float ParseFloatOrDefault(this string input, float defaultValue = 0)
		{
			float ret = defaultValue;

			float.TryParse(input, out ret);

			return ret;
		}

		public static float ClampMagnitude(this float input, float mag)
		{
			return Mathf.Clamp(input, -mag, mag);//Min(Mathf.Abs(input), mag) * Mathf.Sign(input);
		}
	}
}

