using UnityEngine;

public static class ColorExt
{
	public static readonly Color Pink      = new Color(1f, 0.8f, 0.8f);
	public static readonly Color DarkGreen = new Color(0f, 0.66f, 0.0f);
	public static readonly Color DarkBlue  = new Color(0f, 0f, 0.66f);
	public static readonly Color DarkRed   = new Color(0.66f, 0f, 0f);
	public static readonly Color Teal      = new Color(0f, 0.8f, 0.8f);
	public static readonly Color Orange    = new Color(1f, 0.66f, 0f);

	public static void SetAlpha(this Material material, float alpha)
	{
		material.color = material.color.WithAlpha(alpha);
	}

	public static Color WithR(this Color value, float red)
	{
		value.r = red;
		return value;
	}

	public static Color WithG(this Color value, float green)
	{
		value.g = green;
		return value;
	}

	public static Color WithB(this Color value, float blue)
	{
		value.b = blue;
		return value;
	}

	public static Color WithAlpha(this Color value, float alpha)
	{
		value.a = alpha;
		return value;
	}

	/// <summary>
	/// Parses the string as an HTML (and Hex) color. #rgb, #rrggbb, #rgba, #rrggbbaa and some color names (e.g. red
	/// , yellow, white, etc) produce valid results. If the parsing fails, it falls back to the defaultColor param.
	/// </summary>
	public static Color ParseHtmlColor(this string hex, Color defaultColor = default(Color))
	{
		ColorUtility.TryParseHtmlString(hex, out defaultColor);

		return defaultColor;
	}
}
