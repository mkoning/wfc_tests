using UnityEngine;

public static class RectExt
{
	public static float AspectRatio(this Rect rect)
	{
		return rect.width / rect.height;
	}

	public static void Encapsulate(this Rect rect, Vector2 point)
	{
		rect.x = Mathf.Min(rect.x, point.x);
		rect.y = Mathf.Min(rect.y, point.y);

		rect.xMax = Mathf.Max(rect.xMax, point.x);
		rect.yMax = Mathf.Max(rect.yMax, point.y);
	}
}