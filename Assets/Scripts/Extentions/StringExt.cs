using System;

public static class StringExt
{
	public static string TrimLength(this string text, int maxLength, string trimInidicator = "")
	{
		if (text == null) throw new ArgumentNullException(nameof(text));

		return text.Length <= maxLength ? text : $"{text.Substring(0, maxLength)}{trimInidicator}";
	}

	public static string TrimEnd(this string text, int amount)
	{
		if (text == null) throw new ArgumentNullException(nameof(text));
		var length = text.Length - amount;

		return TrimLength(text, length);
	}

	public static bool IsNotNullOrEmpty(this string text)
	{
		return string.IsNullOrEmpty(text) == false;
	}

	public static string TrimFirst(this string text, string search)
	{
		if (text == null) throw new ArgumentNullException(nameof(text));
		if (search == null) throw new ArgumentNullException(nameof(search));
		
		var pos = text.IndexOf(search, StringComparison.CurrentCulture);

		return pos < 0
			       ? text
			       : text.Substring(0, pos) + text.Substring(pos + search.Length);
	}

	public static string ReplaceFirst(this string text, string search, string replace)
	{
		if (text == null) throw new ArgumentNullException(nameof(text));
		if (search == null) throw new ArgumentNullException(nameof(search));
		if (replace == null) throw new ArgumentNullException(nameof(replace));

		var pos = text.IndexOf(search, StringComparison.CurrentCulture);
		
		return pos < 0
			       ? text
			       : text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
	}

	public static string Reversed(this string text)
	{
		if (text == null) throw new ArgumentNullException(nameof(text));

		var array = text.ToCharArray();
		Array.Reverse(array);

		return new string(array);
	}

	public static string With(this string text, string needle, object haystack)
	{
		if (text == null) throw new ArgumentNullException(nameof(text));
		if (needle == null) throw new ArgumentNullException(nameof(needle));

		return text.Replace(needle, haystack.ToString());
	}
}
