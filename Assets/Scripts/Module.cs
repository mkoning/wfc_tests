using System;
using UnityEngine;

[System.Serializable]
public class Module
{
	[SerializeField] private Block _block;
	[SerializeField] private int _rotation;

	public Block Block => _block;
	public int Rotation => _rotation;
	public float Probability => _block.Probability;

	public Module(Block block, int rotation)
	{
		_block = block;
		_rotation = rotation;
	}

	public int GetIdForDirection(Direction direction)
	{
		var faces = Block.Faces.ShiftCopy(Rotation);
		if (direction == Direction.Left) return faces.Left;
		if (direction == Direction.Right) return faces.Right;
		if (direction == Direction.Forward) return faces.Forward;
		if (direction == Direction.Backward) return faces.Backward;

		throw new ArgumentOutOfRangeException(nameof(direction), $"Given value was: {direction}");
	}
}
