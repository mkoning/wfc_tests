using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class ConnectionData : ScriptableObject
{
	[SerializeField] private List<ConnectionId> _connectionIds;

	private static ConnectionData _instance;
	private static ConnectionData Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = Resources.Load<ConnectionData>("Connection Data");
			}

			return _instance;
		}
	}

	public static int GetId(Vector2[] face)
	{
		var ids = Instance._connectionIds;
		
		for (var i = 0; i < ids.Count; i++)
		{
			if (BlockCalc.Match(ids[i].FaceVertices, face))
			{
				return i;
			}
		}

		ids.Add(new ConnectionId(face));
		return ids.Count - 1;
	}
}

[System.Serializable]
public class ConnectionId
{
	public ConnectionId(Vector2[] faceVertices)
	{
		FaceVertices = faceVertices;
	}

	public Vector2 this[int key]
	{
		get
		{
			return FaceVertices[key];
		}
		set
		{
			FaceVertices[key] = value;
		}
	}

	public Vector2[] FaceVertices;
}
