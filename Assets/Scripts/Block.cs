using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Block : MonoBehaviour
{
	public const float BLOCK_SIZE  = 3f;
	[SerializeField][Range(0.01f, 10f)] private float _probability = 1;
	[SerializeField] private FaceInfo _faceInfo;
	[SerializeField] private int[] _rotations;

	public float Probability => _probability / _rotations.Length;
	public FaceInfo Faces => _faceInfo;

	#if UNITY_EDITOR
	public static int CurrentSelectedId = -1;
	private void Reset()
	{
		FindFaces();
		FindRotations();
	}

	void FindFaces()
	{
		var mesh =  GetComponentInChildren<MeshFilter>()?.sharedMesh;
		if (mesh == null)
		{
			Debug.LogError("Failed to find a mesh");
			return;
		}

		_faceInfo = new FaceInfo();
		_faceInfo.Forward = ConnectionData.GetId(BlockCalc.GetFace(mesh, Vector3.forward));
		_faceInfo.Right = ConnectionData.GetId(BlockCalc.GetFace(mesh, Vector3.right));
		_faceInfo.Backward = ConnectionData.GetId(BlockCalc.GetFace(mesh, Vector3.back));
		_faceInfo.Left = ConnectionData.GetId(BlockCalc.GetFace(mesh, Vector3.left));
	}

	private void FindRotations()
	{
		var rotations = new List<(FaceInfo Face, int Rotation)>();

		// The default rotation
		rotations.Add((_faceInfo, 0));

		// The other possible options
		for (int i = 1; i < 4; i++)
		{
			var copy = _faceInfo.ShiftCopy(i);
			if (rotations.Any(t => t.Face.Equals(copy)))
			{
				// skip, we already have a rotation option with this set of faces
				continue;
			}

			rotations.Add((copy, i));
		}

		// Apply the rotations
		_rotations = rotations.Select(t => t.Rotation).ToArray();
	}

	void OnDrawGizmos()
	{
		if (CurrentSelectedId > -1)
		{
			Gizmos.color = Color.yellow;
			for (int i = 0; i < 4; i++)
			{
				if (_faceInfo[i] == CurrentSelectedId)
				{
					Gizmos.DrawSphere(transform.position + DebugPositions.GetPosition(i), 0.5f);
				}
			}
		}
	}
}

// positions from the pivot
public static class DebugPositions
{
	public static Vector3 Forward = Vector3.left * Block.BLOCK_SIZE / 2f;
	public static Vector3 Right = Vector3.back * Block.BLOCK_SIZE / 2f;
	public static Vector3 Backward = (Vector3.back * Block.BLOCK_SIZE) + (Vector3.left * Block.BLOCK_SIZE / 2f);
	public static Vector3 Left = (Vector3.back * Block.BLOCK_SIZE / 2f) + (Vector3.left * Block.BLOCK_SIZE);

	public static Vector3 GetPosition(int index)
	{
		switch (index)
		{
			case 0: return Forward;
			case 1: return Right;
			case 2: return Backward;
			case 3: return Left;
		}
		return Vector3.zero;
	}
}
#endif